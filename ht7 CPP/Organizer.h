#pragma once
#include "typedefs.h"
#include "FileEditor.h"
#include "Note.h"
typedef std::list<Contact *> ContactsCollection;
typedef std::list<Note *> NotesCollection;
enum Data { ContactData, NoteData };
class Organizer
{
private:
	ContactsCollection Contacts;
	NotesCollection Notes;
	void FormatOut(strings &data, Data type);
	void FormatIn(strings data, Data type);
public:
	Organizer();
	void SetContacts(ContactsCollection collect);
	void SetNotes(NotesCollection collect);
	~Organizer();
#pragma region Contacts
	void OpenContacts(string path);
	void SaveContacts(string path);
	void PrintContact(string name);
	Contact * GetContact(string name);
	void DeleteContact(string name);
	void InsertContact(Contact * cont);
	void PrintContacts();
	void FindContact(string sub);
#pragma endregion
#pragma region Notes
	void OpenNotes(string path);
	void SaveNotes(string path);
	void PrintNote(string name);
	Note * GetNote(string name);
	void DeleteNote(string name);
	void InsertNote(Note * cont);
	void PrintNotes();
	void FindNote(string sub);
#pragma endregion	
};

