#include "stdafx.h"
#include "Contact.h"
#include <iostream>

Contact::Contact()
{
}


Contact::Contact(string name, string phone, string mail)
{
	Name = name;
	Phone = phone;
	Email = mail;
}

Contact::~Contact()
{
}
string Contact::ToString() {
	return GetName() +"\t" + GetPhone() + "\t"  + GetEmail() + "\n";
}

void Contact::SetName(string str) { Name = str; }
void Contact::SetPhone(string str) { Phone = str; }
void Contact::SetEmail(string str) { Email = str; }
string Contact::GetName() { return Name; }
string Contact::GetPhone() { return Phone; }
string Contact::GetEmail() { return Email; }
