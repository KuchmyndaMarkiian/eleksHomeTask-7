#include "stdafx.h"
#include "FileEditor.h"
#include <iostream>
#include <fstream>


void FileEditor::ReadData(strings &lines)
{
	try{
		ifstream str(path, std::ofstream::in);
		if (!str.is_open())
			throw;
		
		string line;
		while (getline(str, line))
		{
			lines.push_back(line);
		}
		str.close();
		cout << "Reading is succesfull\n";
	}
	catch(...){
		cout << "Error of reading data\n";
	}	
}

void FileEditor::WriteData(strings lines)
{
	try {
		ofstream str(path, std::ofstream::out | std::ofstream::trunc);
		if (!str.is_open())
			throw;

		string line;
		for each(auto line in lines)
		{
			str << line;
		}
		str.close();
		cout << "Writing is succesfull\n";
	}
	catch (...) {
		cout << "Error of writing data\n";
	}
}

FileEditor::FileEditor()
{
	this->path = "data.txt";
}

FileEditor::FileEditor(string path)
{
	this->path = path;
}


FileEditor::~FileEditor()
{
}
