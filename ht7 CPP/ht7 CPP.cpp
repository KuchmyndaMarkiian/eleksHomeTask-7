// ht7 CPP.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "Organizer.h"
#include <string>
#include <iostream>

int main()
{
	Organizer *organizer = new Organizer();
	//Set data (initialization)
	/*organizer->SetContacts({ new Contact("new", "new", "new") ,new Contact("new", "new", "new") });
	organizer->SetNotes({ new Note("new", "new") ,new Note("new", "new") });*/

	//Open Data
	organizer->OpenContacts("data.txt");
	organizer->OpenNotes("notes.txt");

	//Print all data
	organizer->PrintContacts();
	organizer->PrintNotes();

	organizer->FindContact("k1");
	//Get Contact or note on name
	/*organizer->GetContact("null");
	organizer->GetNote("null");*/

	//Delete
	/*organizer->DeleteContact("null");
	organizer->DeleteNote("null");*/

	//Insert
	/*organizer->InsertContact(new Contact("new", "new", "new"));
	organizer->InsertNote(new Note("new", "new"));*/

	//Save data
	organizer->SaveContacts("data.txt");
	organizer->SaveNotes("notes.txt");

	cin.get();
	delete organizer;
	return 0;
}

