#pragma once
#include "typedefs.h"
typedef std::list<string> strings;
class FileEditor
{
private:
	string path;
public:
	void ReadData(strings &lines);
	void WriteData(strings lines);
	FileEditor();
	FileEditor(string path="data.txt");
	~FileEditor();
};

