#pragma once
#include "typedefs.h"
#include "FileEditor.h"
class Note
{
private:
	string Header;
	string Text;

public:
	void SetHeader(string str);
	void SetText(string str);

	string GetHeader();
	string GetText();
	string ToString();
	Note();
	Note(string header, string text);
	~Note();
};

