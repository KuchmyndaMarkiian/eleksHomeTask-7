#include "stdafx.h"
#include "Note.h"


void Note::SetHeader(string str)
{
	Header = str;
}

void Note::SetText(string str)
{
	Text = str;
}

string Note::GetHeader()
{
	return Header;
}

string Note::GetText()
{
	return Text;
}

Note::Note()
{
}

Note::Note(string header, string text)
{
	Header = header;
	Text = text;
}

string Note::ToString() {
	return GetHeader() + "\t" + GetText() +  "\n";
}
Note::~Note()
{
}
