#pragma once
#include "typedefs.h"
class Contact
{
private:
	string Name;
	string Phone;
	string Email;
public:
	void SetName(string str);
	void SetPhone(string str);
	void SetEmail(string str);

	string GetName();
	string GetPhone();
	string GetEmail();
	Contact();
	Contact(string name, string phone, string mail);
	~Contact();
	string ToString();
};

