#include "stdafx.h"
#include "Organizer.h"
#include <iostream>
#include <sstream>
#include "FileEditor.h"
#include "typedefs.h"

void Organizer::FormatOut(strings &data, Data type)
{
	switch (type)
	{
	case ContactData:
		for each(auto cont in Contacts)
		{
			data.push_back(cont->ToString());
		}
		break;
	case NoteData:
		for each(auto note in Notes)
		{
			data.push_back(note->ToString());
		}
		break;
	default:
		break;
	}
	
}

void Organizer::FormatIn(strings  data, Data type)
{
	string first, second, third;
	for each(auto line in data)
	{
		std::istringstream str(line);
		string token;
		int i = 1;
		switch (type)
		{
			case ContactData:
				while (std::getline(str, token, '\t'))
				{
					switch (i)
					{
						case 1:first = token; break;
						case 2:second = token; break;
						case 3:third = token; break;
					}
					i++;
				}
				Contacts.push_back(new Contact(first, second, third));
				break;
			case NoteData:
			{
				while (std::getline(str, token, '\t'))
				{
					switch (i)
					{
						case 1:first = token; break;
						case 2:second = token; break;
					}						
					i++;
				}
				Notes.push_back(new Note(first, second));
				break;
			}
		}
	}
}

Organizer::Organizer()
{
}

void Organizer::SetContacts(ContactsCollection collect)
{
	Contacts = collect;
}

void Organizer::SetNotes(NotesCollection collect)
{
	Notes = collect;
}

Organizer::~Organizer()
{
}

#pragma region Contact
void Organizer::OpenContacts(string path)
{
	FileEditor* fe = new FileEditor(path);
	strings  data;
	fe->ReadData(data);
	FormatIn(data, ContactData);
}

void Organizer::SaveContacts(string path)
{
	FileEditor* fe = new FileEditor(path);
	strings  data;
	FormatOut(data, ContactData);
	fe->WriteData(data);
}

void Organizer::PrintContact(string index)
{
	auto cont = GetContact(index);
	if (cont != null)
	{
		cout << cont->ToString();
	}
	else {
		cout << "\nContact is not founded" << "\n";
	}
}

Contact * Organizer::GetContact(string name) {

	for each (auto contact in Contacts)
	{
		if (contact->GetName() == name)
		{
			return contact;
		}
	}
	return null;
}

void Organizer::DeleteContact(string name) {
	Contact *deleting;
	for each (auto contact in Contacts)
	{
		if (contact->GetName() == name)
		{
			deleting = contact;
			break;
		}
	}
	Contacts.remove(deleting);
}

void Organizer::InsertContact(Contact * cont) {
	Contacts.push_back(cont);
}

void Organizer::PrintContacts() {
	cout << "Contacts:\n";
	for each (auto contact in Contacts)
	{
		cout << contact->ToString();
	}
	cout << "\n";
}
void Organizer::FindContact(string sub)
{
	for each (auto contact in Contacts)
	{
		auto word = contact->GetName();
		int a = word.find(sub);
		if (a > -1) {
			cout << "word with sub \"" << sub << "\":\t" << contact->ToString();
			cout << "\n";
			return;
		}
	}
	cout << "not founded!\n";
}
#pragma endregion
#pragma region Note
void Organizer::OpenNotes(string path)
{
	FileEditor* fe = new FileEditor(path);
	strings  data;
	fe->ReadData(data);
	FormatIn(data, NoteData);
}

void Organizer::SaveNotes(string path)
{
	FileEditor* fe = new FileEditor(path);
	strings  data;
	FormatOut(data, NoteData);
	fe->WriteData(data);
}

void Organizer::PrintNote(string index)
{
	auto cont = GetNote(index);
	if (cont != null)
	{
		cout << cont->ToString();
	}
	else {
		cout << "\Note is not founded" << "\n";
	}
}

Note * Organizer::GetNote(string name)
{
	for each (auto note in Notes)
	{
		if (note->GetHeader() == name)
		{
			return note;
		}
	}
	return null;
}

void Organizer::DeleteNote(string name)
{
	Note *deleting;
	for each (auto note in Notes)
	{
		if (note->GetHeader() == name)
		{
			deleting = note;
			break;
		}
	}
	Notes.remove(deleting);
}

void Organizer::InsertNote(Note * cont)
{
	Notes.push_back(cont);
}

void Organizer::PrintNotes()
{
	cout << "Notes:\n";
	for each (auto contact in Notes)
	{
		cout << contact->ToString();
	}
	cout << "\n";
}
void Organizer::FindNote(string sub)
{
	for each (auto note in Notes)
	{
		auto word = note->GetHeader();
		int a = word.find(sub);
		if (a > -1) {
			cout << "word with sub \"" << sub << "\":\t" << note->ToString();
			cout << "\n";
			return;
		}
	}
	cout << "not founded!\n";
}
#pragma endregion
